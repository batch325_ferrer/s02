-- SQL syntaxes are not case sensitive.
-- If you are going to add a SQL syntax (capitalize)
-- If you re going to use or add name of a column or table (small letter)



-- TO show all the list of all of databases

SHOW DATABASES;


-- TO create or add a database

CREATE DATABASE music_db;


-- to drop or delete a database

DROP DATABASE music_db;


-- Select a Database

USE music_db;


--command promp
install mariadb
add C:\Program Files\MariaDB 11.0\bin to system evironmental variable path
run command promp 
mysql -u root -p
password 1234

-- Creating a Table
CREATE TABLE users(
	id INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	password VARCHAR(50) NOT NULL,
	full_name VARCHAR(50) NOT NULL,
	contact_number INT NOT NULL,
	email VARCHAR(50),
	address VARCHAR(50),
	PRIMARY KEY (id)
);

-- MIni Activity: Create the artist table:
CREATE TABLE artists(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(50) NOT NULL,
	PRIMARY KEY (id)
);

-- TO change the name of the table
ALTER TABLE table_old_name RENAME table_new_name;
RENAME TABLE table_old_name TO table_new_name;

--fk foreign key
--pk primary key
CREATE TABLE albums(
	id INT NOT NULL AUTO_INCREMENT,
	album_title VARCHAR(50) NOT NULL,
	date_released DATE NOT NULL,
	artist_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_albums_artist_id
		FOREIGN KEY (artist_id) REFERENCES artists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);

CREATE TABLE songs(
	id INT NOT NULL AUTO_INCREMENT,
	song_title VARCHAR(50) NOT NULL,
	song_length INT NOT NULL,
	song_genre VARCHAR(50) NOT NULL,
	album_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_songs_album_id
		FOREIGN KEY (album_id) REFERENCES albums(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);

CREATE TABLE playlists(
	id INT NOT NULL AUTO_INCREMENT,
	user_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlists_user_id
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT		
);


-- MULTIPLE FOREIGN KEYS
CREATE TABLE playlists_songs(
	id INT NOT NULL AUTO_INCREMENT,
	playlist_id INT NOT NULL,
	song_id INT NOT NULL,
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_playlist_songs_playlist_id
		FOREIGN KEY (playlist_id) REFERENCES playlists(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_playlist_songs_song_id
		FOREIGN KEY (song_id) REFERENCES songs(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT			
);















